import java.util.ArrayList;

/**
 * @author D-Abraham
 */
public class Tester {
    public static void main(String[] args) {

        HeapMerge heap1 = new HeapMerge();
        HeapMerge heap2 = new HeapMerge();
        HeapMerge heap3 = new HeapMerge();
        heap1.add(48);
        heap1.add(25);
        heap1.add(19);
        heap1.add(28);
        heap1.add(65);
        heap1.add(35);
        heap2.add(9);
        heap2.add(32);
        heap2.add(64);
        heap2.add(1);

        System.out.println("heap1:\n"+heap1.toString());
        System.out.println("heap2:\n"+ heap2.toString());

        ArrayList<Integer> tmp = heap1.getList();
        for (Integer num :tmp) {
            heap3.add(num);
        }
        tmp=heap2.getList();
        for (Integer num :tmp) {
            heap3.add(num);
        }
        System.out.println("heap3(1&2merged):\n"+ heap3.toString());

    }
}
