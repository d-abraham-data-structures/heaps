import java.util.ArrayList;

/**
 * @author D-Abraham
 */
public class HeapMerge {
    ArrayList<Integer> heap;

    public HeapMerge() {

        heap = new ArrayList<Integer>();
    }

    public ArrayList getList() {

        return heap;
    }
    public int getSize() {

        return heap.size();
    }

    public void add(Integer item) {
        heap.add(item);
        swap(getSize() - 1);
    }

    private void swap(int index) {
        int parent = (index - 1)/2;
        if (heap.get(index).compareTo(heap.get(parent)) < 0) {
            Integer tmp = heap.get(parent);
            heap.set(parent, heap.get(index));
            heap.set(index, tmp);
            swap(parent);
        }
    }

    public String toString() {
        String str = "";
        for (Integer num : heap) {
            str += num.toString() + " ";
        }
        return str;
    }
}
